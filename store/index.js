import * as data from '../static/data.json';
const courses = data.default;

export const state = () => ({
    courses: courses.length ? [...courses.reverse()] : [],
    per_page: [],
    selected: {}
});

export const mutations = {
    ADD_COURSE(state, payload) {
        payload.id = state.courses.length + 1
        state.courses.unshift(payload);
    },
    SELECT_COURSE(state, payload) {
        let index = state.courses.findIndex(course => course.id === +payload)
        if(index !== -1) {
            state.selected = state.courses[index];
        }
    },
    UPDATE_COURSE(state, payload) {
        let index = state.courses.findIndex(course => course.id === +payload.id)
        if(index !== -1) {
            state.courses[index] = payload;
        }
    },
    REMOVE_COURSE(state, payload) {
        let index = state.courses.findIndex(course => course.id === +payload)
        if(index !== -1) {
            state.courses.splice(index, 1);
        }
    },
    SORT_COURSES(state, payload) {
        state.courses = [...state.courses.sort((a, b) => {
            if(payload === 'start_date')
                return new Date(b[payload]) - new Date(a[payload]);
            return b[payload] - a[payload];
        })];
    },
    PER_PAGE(state, payload) {
        state.per_page = state.courses.slice((payload - 1) * 10, 10 * payload);
    }
};

export const actions = {
    ADD_COURSE({commit}, payload) {
        commit('ADD_COURSE', payload);
    },
    UPDATE_COURSE({commit}, payload) {
        commit('UPDATE_COURSE', payload);
    },
    REMOVE_COURSE({commit}, payload) {
        commit('REMOVE_COURSE', payload);
    },
    SELECT_COURSE({commit}, payload) {
        commit('SELECT_COURSE', payload);
    },
    SORT_COURSES({commit}, payload) {
        commit('SORT_COURSES', payload);
    },
    PER_PAGE({commit}, payload) {
        commit('PER_PAGE', payload);
    }
};

export const getters = {
    COURSES: state => state.courses,
    SELECTED: state => state.selected,
    TOTAL: state => state.courses.length,
    COURSES_PER_PAGE: state => state.per_page
};